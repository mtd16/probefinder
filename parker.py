import pandas as pd 
import matplotlib.pyplot as plt 
'exec(%matplotlib inline)'

# Parker Solar Probe Data
parker_url = 'parker_xy.csv'
dfP = pd.read_csv(parker_url, header=None)
dfP = dfP.drop(labels=[0,1,4,5,6,7,8,9,10,11], axis=1)
dfP = dfP.rename(index=str, columns={2:'x',3:'y'})

# Earth Data
earth_url = 'earth_xy.csv'
dfE = pd.read_csv(earth_url, header=None)
dfE = dfE.drop(labels=[0,1,4,5,6,7,8,9,10,11], axis=1)
dfE = dfE.rename(index=str, columns={2:'x',3:'y'})

# Venus Data
venus_url = 'venus_xy.csv'
dfV = pd.read_csv(venus_url, header=None)
dfV = dfV.drop(labels=[0,1,4,5,6,7,8,9,10,11], axis=1)
dfV = dfV.rename(index=str, columns={2:'x',3:'y'})

# Mercury Data
mercury_url = 'mercury_xy.csv'
dfM = pd.read_csv(mercury_url, header=None)
dfM = dfM.drop(labels=[0,1,4,5,6,7,8,9,10,11], axis=1)
dfM = dfM.rename(index=str, columns={2:'x',3:'y'})

# Plotting
plt.figure(figsize=(10,10))
plt.scatter(dfP['x'], dfP['y'], c='black', marker='.', label='Parker')
plt.scatter(dfE['x'], dfE['y'], c='blue', marker='.', label='Earth')
plt.scatter(dfV ['x'], dfV['y'], c='green', marker ='.', label='Venus')
plt.scatter(dfM['x'], dfM['y'], c='red', marker='.', label='Mercury')
plt.scatter(0, 0, s=500, c='orange', marker='*', label='Sun')
plt.grid()
plt.legend()
plt.title('Where is the Parker Solar Probe?')
plt.xlabel('x-position, km')
plt.ylabel('y-position, km')
plt.show()